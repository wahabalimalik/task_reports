# -*- coding: utf-8 -*-
from openerp import http, fields
from openerp.http import request
from datetime import timedelta


class Home(http.Controller):

    @http.route('/task_report/data', type='json', auth='user')
    def task_report(self, **kw):
        return self._get_task_reports(**kw)

    def _get_task_reports(self, **kw):
        data = []
        tasks = request.env['project.task']
        domain = [('user_id', 'in', kw['user_id'])] if len(kw['user_id']) > 0 else []

        # Get Tasks Group By Employees
        group_by_employees = tasks.read_group(domain, fields=['user_id'], groupby=['user_id'])

        for employee_task in group_by_employees:
            # Get Employee Data
            employee_data = request.env['hr.employee'].search_read(
                [('user_id', '=', employee_task['user_id'][0])], ['name', 'job_id', 'work_email', 'user_id'])[0]

            # Get Tasks Group By Projects
            group_by_projects = tasks.read_group(
                employee_task['__domain'],
                fields=['project_id'], groupby=['project_id'])

            task_detail = self.get_task_detail(tasks, group_by_projects)

            employee_data['task_data'] = task_detail
            data.append(employee_data)

        return data

    def get_task_detail(self, tasks, group_by_projects):
        lines = []
        messages_id = request.env['mail.message']
        for project_base in group_by_projects:
            done_stage = request.env.ref("project.project_tt_deployment")
            domain = project_base['__domain']
            task_ids = tasks.search(domain)

            open_tasks = tasks.search_count(domain + [
                ('stage_id', '!=', done_stage.id)
            ])
            delay_tasks = tasks.search_count(domain + [
                ('date_deadline', '<', fields.date.today()),
                ('stage_id', '!=', done_stage.id)
            ])
            finish_month = messages_id.search_count([
                ('res_id', 'in', task_ids.ids),
                ('subtype_id', '=', request.env.ref('project.mt_task_stage').id),
                ('parent_id', '!=', False),
                ('body', 'like', " %s</div>" % str(done_stage.name)),
                ('create_date', '>=', str(fields.date.today().replace(day=1)))
            ])
            finish_week = messages_id.search_count([
                ('res_id', 'in', task_ids.ids),
                ('subtype_id', '=', request.env.ref('project.mt_task_stage').id),
                ('parent_id', '!=', False),
                ('body', 'like', " %s</div>" % str(done_stage.name)),
                ('create_date', '>=', str(fields.date.today() - timedelta(days=fields.date.today().weekday())))
            ])

            lines.append({
                'project': project_base['project_id'][1],
                'open_tasks': open_tasks,
                'delay_tasks': delay_tasks,
                'finish_month': finish_month,
                'finish_week': finish_week,
            })
        return lines

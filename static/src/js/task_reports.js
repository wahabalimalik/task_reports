openerp.task_reports = function (instance) {
    var _t = instance.web._t,
        _lt = instance.web._lt;
    var QWeb = instance.web.qweb;

    instance.web.client_actions.add('task_reports', 'instance.web.TaskReports');

    instance.web.TaskReports = instance.web.Widget.extend({
        template: 'TaskReports',

        events: {
            "click .oe_employee_name": "getEmployeeInfo"
        },

        init: function(parent, dataset, view_id, options) {
            var self = this;
            this.user_id = dataset.params.employeeId ? [parseInt(dataset.params.employeeId)]: false;
            this.action = parent.breadcrumbs.length ? parent.breadcrumbs[0]: false;
            this._super(parent);
        },

        start: function() {
            this._super();
            var self = this;
            this.$el.on('click', 'a.oe_breadcrumb_item', this.on_breadcrumb_clicked);

            this.rpc("/task_report/data", {user_id: this.user_id || []}).done(function(data) {
                this.record = data;
                $('#task_reports_main').append(QWeb.render('EmployeeTaskDetail', {"records": data}));
            });
        },

        on_breadcrumb_clicked: function(ev) {
            var self = this;
            self.do_action(this.action.action);
        },

        getEmployeeInfo: function(e) {
            var self = this;
            self.do_action({
                type: 'ir.actions.act_window',
                res_model: 'hr.employee',
                res_id: parseInt(e.target.id),
                views: [[false, 'form']],
                target: 'new',
                flags: {'initial_mode': 'view'}
            }, {
                on_reverse_breadcrumb: function () {
                    this.reload();
                },
            });
        },
    });

    instance.web_kanban.KanbanRecord.include({
        start: function() {
            var self = this;
            this.$el.on('click', '.oe_dropdown_menu li a', function(ev) {
                var fn = 'do_action_' + $(ev.target).data().action;
                if (typeof(self[fn]) === 'function') {
                    self[fn]($(ev.target));
                }
            });
            this.$el.on('click', '.kanban_avatar_link', function(ev) {
                var fn = 'do_action_' + $(ev.target).data().action;
                if (typeof(self[fn]) === 'function') {
                    self[fn]($(ev.target));
                }
            });
            this._super();
        },

        do_action_task_reports: function(e) {
            var self = this;
            self.do_action({
                type: 'ir.actions.client',
                tag: 'task_reports',
                params: {employeeId: e.data().employeeId}
            }, {});
        }
    });

    instance.web_kanban.KanbanGroup.include({

        do_action_task_reports: function() {
            var self = this;
            self.do_action({
                type: 'ir.actions.client',
                tag: 'task_reports',
                params: {}
            }, {});
        }
    });

    instance.web.ViewManager.include({
        do_create_view: function(view_type) {
        // Lazy loading of views
        var self = this;
        var view = this.views[view_type];
        var viewclass = this.registry.get_object(view_type);
        var options = _.clone(view.options);
        if (view_type === "form" && this.action && (this.action.target == 'new' || this.action.target == 'inline')) {
            if (this.action.flags['initial_mode'] === undefined){
                options.initial_mode = 'edit';
            }
        }
        var controller = new viewclass(this, this.dataset, view.view_id, options);

        controller.on('history_back', this, function() {
            var am = self.getParent();
            if (am && am.trigger) {
                return am.trigger('history_back');
            }
        });

        controller.on("change:title", this, function() {
            if (self.active_view === view_type) {
                self.set_title(controller.get('title'));
            }
        });

        if (view.embedded_view) {
            controller.set_embedded_view(view.embedded_view);
        }
        controller.on('switch_mode', self, this.switch_mode);
        controller.on('previous_view', self, this.prev_view);

        var container = this.$el.find("> div > div > .oe_view_manager_body > .oe_view_manager_view_" + view_type);
        var view_promise = controller.appendTo(container);
        this.views[view_type].controller = controller;
        return $.when(view_promise).done(function() {
            self.views[view_type].deferred.resolve(view_type);
            if (self.searchview
                    && self.flags.auto_search
                    && view.controller.searchable !== false) {
                self.searchview.ready.done(self.searchview.do_search);
            } else {
                self.view_completely_inited.resolve();
            }
            self.trigger("controller_inited",view_type,controller);
        });
    },
    });
};
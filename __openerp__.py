# -*- coding: utf-8 -*-
{
    'name' : 'Task Reports',
    'version' : '1.1',
    'author' : 'Wahab Ali Malik || BathroomByDesign',
    'category' : 'Project Management',
    'description' : """
Track multi-level projects, tasks, work done on tasks
=====================================================

This application allows an operational project management system to organize your activities into tasks and plan the work you need to get the tasks completed.

Gantt diagrams will give you a graphical representation of your project plans, as well as resources availability and workload.

Dashboard / Reports for Project Management will include:
--------------------------------------------------------
* My Tasks
* Open Tasks
* Tasks Analysis
* Cumulative Flow
    """,
    'website': 'https://www.bathroombydesign.com',
    'depends' : ['project', 'hr'],
    'data': [
        'views/task_report_template.xml',
        'views/project_view.xml',
    ],
    'qweb': [
        "static/src/xml/task_reports.xml",
    ],
    'installable': True,
    'auto_install': False,
}
